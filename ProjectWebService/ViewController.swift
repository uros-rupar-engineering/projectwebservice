//
//  ViewController.swift
//  ProjectWebService
//
//  Created by uros.rupar on 6/7/21.
//

import UIKit

class ViewController: UIViewController {
    
    let url = URL(string: "https://swapi.dev/api/vehicles/")!
    
    var task : URLSessionTask!
    
    let  decoder = JSONDecoder()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        task = URLSession.shared.dataTask(with: url){ (data,response, error) in
            
            if let data = data, let s = String(data:data,encoding: String.Encoding.utf8) {
                
                let vehicle = try? decoder.decode(FullVehicle.self, from: data)
                
            
            }
        }
        
    }
    
    func getData(){
        task.resume()
    }
    
    
    @IBAction func getdataButton(_ sender: Any) {
        
        getData()
        
    }
    
    
    
    
    
}

